package models

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"log"
)

type RoomBasic struct {
	Identity     string `bson:"identity"`
	Number       int    `bson:"number"` //房间人数
	Name         string `bson:"name"`   //房间名字
	Info         string `bson:"info"`
	RoomType     int    `bson:"room_type"` // 房间 类型 【1-独聊房间 2-群聊房间】
	UserIdentity string `bson:"user_identity"`
	CreatedAt    int64  `bson:"created_at"`
	UpdatedAt    int64  `bson:"updated_at"`
}

func (RoomBasic) CollectionName() string {
	return "room_basic"
}

func InsertRoomBasic(rb *RoomBasic) error {
	_, err := MongoDB.Collection(RoomBasic{}.CollectionName()).InsertOne(context.Background(), rb)
	return err
}

func DeleteRoomBasic(roomIdentity string) error {
	_, err := MongoDB.Collection(RoomBasic{}.CollectionName()).DeleteOne(context.Background(), bson.M{"identity": roomIdentity})
	return err
}

func JudgeRoomTypeIs2(roomIdentity string) bool {
	var rb RoomBasic
	err := MongoDB.Collection(RoomBasic{}.CollectionName()).FindOne(context.Background(), bson.M{"identity": roomIdentity}).Decode(&rb)
	if err != nil {
		log.Printf("[DB error] %v", err)
		return false
	}
	if rb.RoomType == 2 {
		return true
	} else {
		return false
	}
}

func IncreaseNumber(identity string) error {
	_, err := MongoDB.Collection(RoomBasic{}.CollectionName()).UpdateOne(context.Background(), bson.M{"identity": identity}, bson.M{"$inc": bson.M{"number": 1}})
	return err
}
