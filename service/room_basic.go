package service

import (
	"github.com/gin-gonic/gin"
	"im/models"
	"im/utils"
	"log"
	"net/http"
	"time"
)

//新建群聊
func CreateRoom(c *gin.Context) {
	roomName := c.PostForm("roomName")
	roomInfo := c.PostForm("roomInfo")
	if roomName == "" {
		roomName = "新群聊"
	}
	if roomInfo == "" {
		roomInfo = "无介绍信息"
	}
	//添加room_basic
	uc := c.MustGet("user_claims").(*utils.MyClaims)
	rb := &models.RoomBasic{
		Identity:     utils.GetUUID(),
		Number:       1,
		Name:         roomName,
		Info:         roomInfo,
		RoomType:     2,
		UserIdentity: uc.Identity,
		CreatedAt:    time.Now().Unix(),
		UpdatedAt:    time.Now().Unix(),
	}
	err := models.InsertRoomBasic(rb)
	if err != nil {
		log.Printf("[DB error] %v", err)
		c.JSON(http.StatusOK, gin.H{
			"code": -1,
			"msg":  "系统错误",
		})
		return
	}
	//群主添加user_room
	ur := &models.UserRoom{
		UserIdentity: uc.Identity,
		RoomIdentity: rb.Identity,
		RoomType:     2,
		CreatedAt:    time.Now().Unix(),
		UpdatedAt:    time.Now().Unix(),
	}
	err = models.InsertUserRoom(ur)
	if err != nil {
		log.Printf("[DB error] %v", err)
		c.JSON(http.StatusOK, gin.H{
			"code": -1,
			"msg":  "系统错误",
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"code": 200,
		"msg":  "新建群聊成功",
		"data": rb,
	})
}

//进入房间
func IntoRoom(c *gin.Context) {
	roomId := c.PostForm("roomIdentity")
	uc := c.MustGet("user_claims").(*utils.MyClaims)
	//判断roomId是否是群聊号
	if !models.JudgeRoomTypeIs2(roomId) {
		c.JSON(http.StatusOK, gin.H{
			"code": -1,
			"msg":  "该房间号不是群聊号",
		})
		return
	}
	//查看自己是否已经在房间了
	if models.IsUserInRoom(uc.Identity, roomId) {
		c.JSON(http.StatusOK, gin.H{
			"code": -1,
			"msg":  "您已经在群聊中",
		})
		return
	}
	//加入房间
	ur := &models.UserRoom{
		UserIdentity: uc.Identity,
		RoomIdentity: roomId,
		RoomType:     2,
		CreatedAt:    time.Now().Unix(),
		UpdatedAt:    time.Now().Unix(),
	}
	err := models.InsertUserRoom(ur)
	if err != nil {
		log.Printf("[DB error] %v", err)
		c.JSON(http.StatusOK, gin.H{
			"code": -1,
			"msg":  "系统错误",
		})
		return
	}
	//修改room_basic的number
	err = models.IncreaseNumber(roomId)
	if err != nil {
		log.Printf("[DB error] %v", err)
		c.JSON(http.StatusOK, gin.H{
			"code": -1,
			"msg":  "系统错误",
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"code": 200,
		"msg":  "加入群聊成功",
	})
}
