package service

import (
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"im/common"
	"im/models"
	"im/utils"
	"log"
	"net/http"
	"time"
)

var upgrader = websocket.Upgrader{} // use default options

var ws = make(map[string]*websocket.Conn)

func WebsocketMessage(c *gin.Context) {
	conn, err := upgrader.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"code": -1,
			"msg":  "系统异常：" + err.Error(),
		})
		return
	}
	defer conn.Close()
	uc := c.MustGet("user_claims").(*utils.MyClaims)
	ws[uc.Identity] = conn
	//添加一个关闭事件的监听器,如果断开了连接，则将连接从map中移除
	conn.SetCloseHandler(func(code int, text string) error {
		log.Printf("Connection closed: %d %s\n", code, text)
		//从ws集合中删除该连接
		delete(ws, uc.Identity)
		return nil
	})
	for {
		var msg common.MessageStruct
		err := conn.ReadJSON(&msg)
		if err != nil {
			log.Printf("Read Error:%v\n", err)
			return
		}
		//todo:判断用户是否属于它发送的消息体里包含的房间
		_, err = models.GetUserRoomByUserIdentityRoomIdentity(uc.Identity, msg.RoomIdentity)
		if err != nil {
			log.Printf("UserIdentity:%v RoomIdentity:%v Not Exits\n", uc.Identity, msg.RoomIdentity)
			return
		}
		//todo:保存消息
		mb := &models.MessageBasic{
			UserIdentity: uc.Identity,
			RoomIdentity: msg.RoomIdentity,
			Data:         msg.Message,
			CreatedAt:    time.Now().Unix(),
			UpdatedAt:    time.Now().Unix(),
		}
		err = models.InsertMessageBasic(mb)
		if err != nil {
			log.Printf("[DB ERROR]:%v\n", err)
			return
		}
		//todo:获取该房间的在线用户
		userRooms, err := models.GetUserRoomByRoomIdentity(msg.RoomIdentity)
		if err != nil {
			log.Printf("[DB ERROR]:%v\n", err)
			return
		}
		for _, ur := range userRooms {
			if cc, ok := ws[ur.UserIdentity]; ok {
				err := cc.WriteMessage(websocket.TextMessage, []byte(msg.Message))
				if err != nil {
					log.Printf("Write Error:%v\n", err)
					return
				}
			}
		}
	}
}
