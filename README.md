### 项目介绍
im 一个用gin实现的简单的即时通讯系统后端

### 技术栈
go语言，gin框架，mongodb，jwt,websocket,redis

### 用到的依赖包

- go get github.com/gorilla/websocket
//websocket核心包
- go get -u github.com/gin-gonic/gin
//gin框架
- go get go.mongodb.org/mongo-driver/mongo
//mongoDB驱动
- go get -u github.com/golang-jwt/jwt/v5
  //jwt权限验证
- go get github.com/satori/go.uuid
//uuid相关
- go get github.com/jordan-wright/email 
//邮箱发送验证码相关
- go get github.com/go-redis/redis/v8
//redis驱动，用来存储验证码

### 功能模块
* 用户模块
* 密码登录
* 发送验证码
* 用户注册
* 用户详情
* 查询用户个人资料
* 添加好友
* 删除好友
* 通讯模块（核心）,单聊，群聊
* 使用HTTP搭建Websocket服务
* 使用GIN搭建Websocket服务
* 发送、接受消息
* 聊天记录列表

具体的功能接口查看路由 im/ router / router.go 