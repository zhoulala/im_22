package utils

import (
	"crypto/tls"
	"fmt"
	"github.com/jordan-wright/email"
	uuid "github.com/satori/go.uuid"
	"im/common"
	"math/rand"
	"net/smtp"
	"strconv"
	"time"
)

//验证码相关
// 生成验证码
func GetCode() string {
	rand.Seed(time.Now().UnixNano())
	res := ""
	for i := 0; i < 6; i++ {
		res += strconv.Itoa(rand.Intn(10))
	}
	return res
}

// 发送验证码
func SendCode(toUserEmail, code string) error {
	e := email.NewEmail()
	e.From = "Get <17775534495@163.com>"
	e.To = []string{toUserEmail}
	e.Subject = "[GoIM]验证码已发送，请查收"
	e.HTML = []byte("您的验证码：<b>" + code + "</b> 5分钟内有效")
	return e.SendWithTLS("smtp.163.com:465",
		smtp.PlainAuth("", "17775534495@163.com", common.EmailPassword, "smtp.163.com"),
		&tls.Config{InsecureSkipVerify: true, ServerName: "smtp.163.com"})
}

// 生成唯一码
func GetUUID() string {
	u := uuid.NewV4()
	return fmt.Sprintf("%x", u)
}
